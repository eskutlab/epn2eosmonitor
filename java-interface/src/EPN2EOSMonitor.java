import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.WriteApiBlocking;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;

import lia.Monitor.JiniClient.Store.Main;
import lia.Monitor.monitor.AccountingResult;
import lia.Monitor.monitor.DataReceiver;
import lia.Monitor.monitor.ExtResult;
import lia.Monitor.monitor.MFarm;
import lia.Monitor.monitor.Result;
import lia.Monitor.monitor.ShutdownReceiver;
import lia.Monitor.monitor.eResult;
import lia.Monitor.monitor.monPredicate;
import java.util.*;

public class EPN2EOSMonitor {
	private static final Logger logger = Logger.getLogger(EPN2EOSMonitor.class.getName());
	public static InfluxDBClient influxDB = null;
	public static WriteApiBlocking writeApi = null;

	public static void main(String[] args){
		String url = null;
		String org = null;
		String bucket = null;
		String token = null;
		try (InputStream input = new FileInputStream("./conf/influxdb.properties")) {

			Properties prop = new Properties();
			prop.load(input);

			// get the property value and print it out
			url = prop.getProperty("influxdb.url");
			org = prop.getProperty("influxdb.org");
			bucket = prop.getProperty("influxdb.bucket");
			token = prop.getProperty("influxdb.token");

		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}

		logger.log(Level.WARNING, url);
		logger.log(Level.WARNING, org);
		logger.log(Level.WARNING, bucket);
		logger.log(Level.WARNING, token);

		if (url == null)
			url = "http://localhost:8086";

		if (org == null)
			influxDB = InfluxDBClientFactory.create(url);
		else
			influxDB = InfluxDBClientFactory.create(url, token.toCharArray(), org, bucket);

		if (influxDB == null) {
			logger.log(Level.INFO, "Could not init DB");
			return;
		}

		writeApi = influxDB.getWriteApiBlocking();

		try {
			FileHandler fh;
			// This block configure the logger with handler and formatter
			fh = new FileHandler("log/app");
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
			logger.setUseParentHandlers(false);
			logger.setLevel(Level.FINEST);
			logger.log(Level.INFO, "finished logging");

		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}

		logger.log(Level.INFO, "App is starting");
		logger.log(Level.WARNING, influxDB.toString());
		logger.log(Level.WARNING, "Version " + influxDB.version());

		// start the repository service
		final Main jClient = new Main();
		logger.log(Level.WARNING, "Started client");
		//Load cluster_to_monitor from config.properties
		String cluster = null;
		String farm = null;
		try (InputStream input = new FileInputStream("./conf/config.properties")) {

			Properties prop_conf = new Properties();
			prop_conf.load(input);

			// get the property value and print it out
			cluster = prop_conf.getProperty("config.cluster");
			farm = prop_conf.getProperty("config.farm");
			

		} catch (SecurityException | IOException e) {
			e.printStackTrace();
		}                 

		if (cluster == null) {
			logger.log(Level.WARNING, "Cluster to monitor not set");
			return;
		}
		if (farm == null) {
			logger.log(Level.WARNING, "Farm not set");
			return;
		}  
                                      
                		
		// register a MyDataReceiver object to receive any new information.
		jClient.addDataReceiver(new MyDataReceiver(cluster));
		logger.log(Level.WARNING, "Receiver added successfully");

		boolean ret;
		
		logger.log(Level.WARNING, "Connected to farm " + farm);
		
		// Register a predicate for the farm
		ret = jClient.registerPredicate(new monPredicate(farm, cluster, "*", -1, -1,
				new String[]{"*"}, null));
		if (!ret)
			logger.log(Level.WARNING, "predicate for " + farm + " not registered");
		
		}

	/**
	 * This is a very simple data receiver that puts some filters on the received data
	 * and outputs the matching values on the console.
	 */
	private static class MyDataReceiver implements DataReceiver, ShutdownReceiver {

	    String cluster;
		MyDataReceiver(String cluster_to_monitor) {
			logger.log(Level.FINEST, "In Receiver contructor");
			this.cluster = cluster_to_monitor;
		}

		public void Shutdown(){
			System.out.flush();
		}

		public void addResult(eResult r) {
			logger.log(Level.FINEST, r.toString());
		}

		//time=$val Farm=$val Node=$val $param_name=$param ...
	    private void buildMessageEos(Result r, String cluster) {

			Point p = Point.measurement(cluster);
			p.addTag("NodeName", r.NodeName);
			p.time(r.getTime(), WritePrecision.MS);
			for (int i = 0; i < r.param.length; i++){
				p.addField(r.param_name[i], r.param[i]);
			}

			writeApi.writePoint(p);

		}
		/*      We received a new entry
		 * */
		public void addResult(Result r){
			logger.log(Level.FINEST, r.toString());
			if (r.ClusterName.equals(this.cluster)) {
			    buildMessageEos(r, r.ClusterName);
			}

		}

		public void addResult(ExtResult er){
			logger.log(Level.FINEST, er.toString());
		}

		public void addResult(AccountingResult ar){
			logger.log(Level.FINEST, ar.toString());
		}

		public void updateConfig(MFarm f){}
	}
}
